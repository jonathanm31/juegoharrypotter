﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;
using System;
//using Proyect.Utility;

namespace Proyect.Networking
{
    [Serializable]
    public class Caldero
    {
        public string turnoJuego;
        public int estado;
        public float tiempo;

        public string idjugador1;
        public string idjugador2;
        public int elemento;

        public bool isTurno = false;
        public Caldero()
        {
            this.estado = 1;
            this.tiempo = 0;
        }

        public void cambiarEstado(int estado)
        {
            this.estado = estado;
        }
        public void errorCaldero()
        {
            this.estado += 5;
        }
    }

}