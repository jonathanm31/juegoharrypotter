﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;
using System;
//using Proyect.Utility;

namespace Proyect.Networking
{
    [Serializable]
    public class Elemento
    {
        public int id;
        public float time;
        public Material material;
        public bool compartido = false;
    }

}