﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;
using System;

//using Proyect.Utility;
namespace Proyect.Networking
{


    [Serializable]
    public class Player
    {
        public string id;
        public Elemento elemento1;
        public Elemento elemento2;
        public string jugador;
        public int rol;
        public bool isHisTurn = false;
    }
}

