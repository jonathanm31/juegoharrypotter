﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;
using System;

//using Proyect.Utility;
namespace Proyect.Networking
{
   
    [Serializable]
    public class Rol
    {
        public int  id;
        public string propiedades;
        public Color color;
    }
}