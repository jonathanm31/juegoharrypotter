﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;
using System;
using UnityEngine.UI;
//using Proyect.Utility;

namespace Proyect.Networking
{
    public class NetworkClient : SocketIOComponent
    {
        // Start is called before the first frame update
        [Header("Network Client")]
        [SerializeField]
        private Transform networkcontainer;
        [SerializeField]
        private GameObject playerPrefab;
        [SerializeField]
        public CalderoObject calderoobject;
        [SerializeField]
        public Text turnoJugador;
        public Text nombreJugador;

        [SerializeField]
        public Canvas esperandoCanvas;
        public Canvas puntajesCanvas;
        private bool empezar;

        [SerializeField]
        public ElementoObject Elemento1;
        public ElementoObject Elemento2;

        private int oldstate;
        private bool turno = false;
        public static string ClienteID { get; private set; }
        private MaquinadeEstados maquinadeEstados;
        private Dictionary<string, NetworkIdentity> serverObjects;
        private bool compartio = false;

        void Start()
        {
            base.Start();
            initializate();
            setupEvents();
            esperandoCanvas.gameObject.SetActive(true);
            puntajesCanvas.gameObject.SetActive(false);

        }

        private void initializate()
        {
            serverObjects = new Dictionary<string, NetworkIdentity>();
            oldstate = calderoobject.caldero.estado;
            maquinadeEstados = GetComponent<MaquinadeEstados>();
        }


        // Update is called once per frame
        void checkTurno()
        {

            if (calderoobject.caldero.turnoJuego == ClienteID)
            {
                turno = true;
            }
            else
            {
                turno = false;
            }
        }

        // Update is called once per frame  - Check if Colider is in another GameObject
        void Update()
        {
           
            base.Update();
            if (!calderoobject.empezar && serverObjects.Count == 2) { 
                calderoobject.empezar = true;
                esperandoCanvas.gameObject.SetActive(false);
                puntajesCanvas.gameObject.SetActive(true);

                if(calderoobject.caldero.turnoJuego == ClienteID)
                {
                    calderoobject.caldero.isTurno = true;
                }
                else
                {
                    calderoobject.caldero.isTurno = false;
                }
            }

            
            
            checkTurno();
            if (serverObjects.Count > 0)
            {
                nombreJugador.text = serverObjects[ClienteID].GetNombre();
                turnoJugador.text = "Turno de:" + serverObjects[calderoobject.caldero.turnoJuego].GetNombre();
                Debug.Log("Turno de:" + serverObjects[calderoobject.caldero.turnoJuego].GetNombre().ToString());
            }
            /*if (!turno && calderoobject.caldero.estado > 1)
            {
                Debug.LogWarning("No es tu turno");
                calderoobject.caldero.estado = 5;
            }*/
            if (turno && oldstate != calderoobject.caldero.estado && serverObjects.Count > 0)
            {
                Debug.LogWarning("Cambio la pocion " + oldstate.ToString() + " -> " + calderoobject.caldero.estado.ToString());
                NetworkIdentity ni = serverObjects[ClienteID];
                ni.GetSocket().Emit("estadoCaldero", new JSONObject(JsonUtility.ToJson(calderoobject.caldero)));
                oldstate = calderoobject.caldero.estado;
            }
            if(!compartio && calderoobject.caldero.elemento > 0)
            {
                Debug.LogWarning("Se envio elemento 1");
                NetworkIdentity ni = serverObjects[ClienteID];
                Compartir compartir = new Compartir();
                compartir.elemento = Elemento1.elemento.id;
                ni.GetSocket().Emit("enviarElemento", new JSONObject(JsonUtility.ToJson(compartir)));
                compartio = true;

            }
           
        }

        private void setupEvents()
        {
            On("open", (E) =>
            {
                Debug.Log("connección establecida");

            });

            On("register", (E) =>
            {
                ClienteID = E.data["id"].ToString().Replace("\"", "");
                Debug.LogFormat("Mi Id de jugador ({0})", ClienteID);
                Debug.LogWarning(E.ToString());
            });

            On("spawn", (E) =>
            {
                string id = E.data["id"].ToString().Replace("\"", "");
                int  rol = Int32.Parse(E.data["rol"].ToString().Replace("\"", ""));
                GameObject go = Instantiate(playerPrefab, networkcontainer);
                go.name = string.Format("Player ({0})", id);
                NetworkIdentity ni = go.GetComponent<NetworkIdentity>();
                ni.SetControllerID(id);
                ni.SetSocketReference(this);
                string nombre = E.data["jugador"].ToString().Replace("\"", "");
                ni.SetNombre(nombre);
                ni.rol = rol;
                int idelemento1 = Int32.Parse(E.data["elemento1"].ToString().Replace("\"", ""));
                float time1 = 0;

                int idelemento2 = Int32.Parse(E.data["elemento2"].ToString().Replace("\"", ""));
                float time2 = 0;

                ni.SetElemento1(idelemento1, time1);
                ni.SetElemento2(idelemento2, time2);

                Elemento1.elemento.id = idelemento1;
                Elemento2.elemento.id = idelemento2;

                serverObjects.Add(id, ni);

            });

            On("turno", (E) =>
            {


            });

            On("disconnected", (E) =>
            {
                string id = E.data["id"].ToString().Replace("\"", "");
                GameObject go = serverObjects[id].gameObject;
                Destroy(go);
                serverObjects.Remove(id);

            });

            On("enviarElemento", (E) =>
             {
                 Debug.Log("Compartir Elemento");
                 string id = E.data["id"].ToString().Replace("\"", "");
                 if(id == ClienteID)
                 {
                     int idelemento2 = Int32.Parse(E.data["elemento2"].ToString().Replace("\"", ""));
                     Elemento2.elemento.id = idelemento2;
                 }
                 
             });
             
            On("estadoCaldero", (E) =>
            {                
                int estado = Int32.Parse(E.data["estado"].ToString().Replace("\"", ""));
                string turno = E.data["turnoJuego"].ToString().Replace("\"", "");
                float tiempo = float.Parse(E.data["tiempo"].ToString().Replace("\"", ""));
                calderoobject.caldero.estado = estado;
                calderoobject.caldero.turnoJuego = turno;
                calderoobject.caldero.tiempo = tiempo;
                serverObjects[turno].SetTurno(true);

                calderoobject.caldero.cambiarEstado(estado);
                
                maquinadeEstados.ActivarEstado();
                turnoJugador.text = "Turno de:"+ serverObjects[turno].GetNombre();
                Debug.Log("cambio estado del caldero a "+estado.ToString());
                Debug.Log("Turno de:" + serverObjects[turno].GetNombre().ToString());
            });


        }
    }

    public class Compartir
    {
        public int elemento;
    }

}

