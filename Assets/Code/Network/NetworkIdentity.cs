﻿using Proyect.Utility.Attribute;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;

namespace Proyect.Networking
{
    public class NetworkIdentity : MonoBehaviour
    {
        [Header("Helpful Values")]
        [SerializeField]
        [GreyOut]
        private string id;
        public int rol;
        public string jugador;
        public Elemento elemento1;
        public Elemento elemento2;
        public bool turno;



        [SerializeField]
        [GreyOut]
        private bool isControlling;


        private SocketIOComponent socket;

        public void Awake()
        {
            isControlling = false;
        }

        public void SetControllerID(string ID)
        {
            id = ID;
            //check if the current ID is the same that we save from the server
            isControlling = (NetworkClient.ClienteID == ID) ? true : false;
        }

        public void SetNombre(string nombre)
        {
            this.jugador = nombre;
        }

        public string GetNombre()
        {
            return this.jugador;
        }

        public void SetElemento1(int id,float time)
        {
            this.elemento1 = new Elemento();
            this.elemento1.id = id;
            this.elemento1.time = time;
        }
        public void SetElemento2(int id, float time)
        {
            this.elemento2 = new Elemento();
            this.elemento2.id = id;
            this.elemento2.time = time;
        }

        public void SetTurno(bool turno)
        {
            this.turno = turno;
        }

        public void SetSocketReference(SocketIOComponent Socket)
        {
            socket = Socket;
        }

        public string GetID()
        {
            return id;
        }

        public bool IsControlling()
        {
            return isControlling;
        }

        public SocketIOComponent GetSocket()
        {
            return socket;
        }

    }
}
