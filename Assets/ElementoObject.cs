﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Proyect.Networking
{
    public class ElementoObject : MonoBehaviour
    {
        // Start is called before the first frame update
        [SerializeField]
        public Elemento elemento;
        public GameObject cylinder;
        public Material material0;
        public Material material1;
        public Material material2;
        public bool compartir;
        void Start()
        {
            elemento = new Elemento();
            compartir = false;

        }

        // Update is called once per frame
        void Update()
        {
            Renderer fluido_render = cylinder.GetComponent<Renderer>();
            if (elemento.id == 0)
            {               
                fluido_render.material = material0;
            }else if(elemento.id == 1)
            {
                fluido_render.material = material1;
            }
            else if(elemento.id == 2)
            {
                fluido_render.material = material2;
            }
        }
    }
}