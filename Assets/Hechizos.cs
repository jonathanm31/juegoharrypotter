﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using System.Linq;
using System;
using System.Text;
using UnityEngine.UI;

public class Hechizos : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    public ParticleSystem AguaAnimacion;
    public ParticleSystem FuegoAnimacion;
    public ParticleSystem AireAnimacion;
    public ParticleSystem Circulo;

    public Text texto;
    private string[] m_Keywords;
    private KeywordRecognizer m_Recognizer;
    private Dictionary<string, Action> actions = new Dictionary<string, Action>();


    void Start()
    {
        Debug.Log("trigger");
        AguaAnimacion.Stop();
        FuegoAnimacion.Stop();
        AireAnimacion.Stop();
       /* Renderer renderGargola = Gargola.GetComponent<Renderer>();
        renderGargola.enabled = false;*/
        Circulo.Stop();
        actions.Add("lumus", Lumus);
        actions.Add("nox", Nox);

        actions.Add("accio", Accio);

        actions.Add("fuego", Fuego);
        actions.Add("aire", Aire);
        actions.Add("tierra", Tierra);
        m_Recognizer = new KeywordRecognizer(actions.Keys.ToArray());

        // m_Recognizer = new KeywordRecognizer(m_Keywords);
        m_Recognizer.OnPhraseRecognized += OnPhraseRecognized;
        m_Recognizer.Start();
    }

    private void OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        StringBuilder builder = new StringBuilder();
        builder.AppendFormat("{0} ({1}){2}", args.text, args.confidence, Environment.NewLine);
        builder.AppendFormat("\tTimestamp: {0}{1}", args.phraseStartTime, Environment.NewLine);
        builder.AppendFormat("\tDuration: {0} seconds{1}", args.phraseDuration.TotalSeconds, Environment.NewLine);
        Debug.Log(builder.ToString());
        actions[args.text].Invoke();

    }

    private void Lumus()
    {
        FuegoAnimacion.Stop();
        AireAnimacion.Stop();

        AguaAnimacion.Play();
        Circulo.Play();
       // texto.text = "Agua";
    }
    private void Nox()
    {
        FuegoAnimacion.Stop();
        AireAnimacion.Stop();

        AguaAnimacion.Play();
        Circulo.Stop();
        // texto.text = "Agua";
    }
    private void Accio()
    {
        FuegoAnimacion.Stop();
        AireAnimacion.Stop();


       /* Renderer renderGargola = Gargola.GetComponent<Renderer>();
        renderGargola.enabled = true;*/
        AguaAnimacion.Play();
        Circulo.Stop();
        // texto.text = "Agua";
    }
    private void Fuego()
    {
        AguaAnimacion.Stop();
        AireAnimacion.Stop();
        FuegoAnimacion.Play();
    //    texto.text = "Fuego";
    }
    private void Aire()
    {
        AguaAnimacion.Stop();
        FuegoAnimacion.Stop();
        AireAnimacion.Play();
    //    texto.text = "Aire";
    }
    private void Tierra()
    {

    }

}
