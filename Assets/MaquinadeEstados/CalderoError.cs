﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CalderoError : MonoBehaviour
{
    [SerializeField]
    public Material m_Material;
    public GameObject Fluido;
    public GameObject m_Image;
    public Sprite m_Sprite;
    private float timeStart = 0;
    public Text timerText;

    private int scoreStart;
    public Text scoreText;
    void Start()
    {
        Debug.LogWarning("despierto ERROR");
        /***TEXTO TIEMPO**/
        timerText.text = timeStart.ToString();

        /**IMAGEN DE POSCION***/
        Image imagen = m_Image.GetComponent<Image>();
        imagen.sprite = m_Sprite;
        imagen.SetNativeSize();

        scoreStart = int.Parse(scoreText.text);
        scoreStart -= 1;


        scoreText.text = scoreStart.ToString();

    }
}
