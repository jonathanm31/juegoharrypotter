﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Proyect.Networking
{
    public class CalderoPosion1 : MonoBehaviour
    {
        private MaquinadeEstados maquinadeEstados;

        [SerializeField]
       
        public GameObject Caldero;
        public GameObject Posion1;
        public GameObject Posion2;
        public AudioSource postionsound;

        public GameObject Compartir;
        [SerializeField]
        public ElementoObject Elemento1;
        public ElementoObject Elemento2;
        private Vector3 calderoPosition = new Vector3();
        private Vector3 elemento1Position = new Vector3();
        private Vector3 elemento2Position = new Vector3();
        private Vector3 compartirVec = new Vector3();
        private float heatingDistance = 0.8f;

        private Material materia_anterior;
        public ParticleSystem fuego;
        private float timeStart = 300;
        public Text timerText;
        public GameObject m_Image;
        public Sprite m_Sprite;
        private bool isOnTime = true;

        public CalderoObject calderoobject;

        private bool turno = false;
        public static string ClienteID { get; private set; }
        private float distancia_posion(Vector3 punto1, Vector3 punto2)
        {
            return Mathf.Sqrt(Mathf.Pow(punto1.y - punto2.y, 2));
        }


        void Start()
        {
            Debug.LogWarning("despierto posion1");
            /***TEXTO TIEMPO**/
            timerText.text = timeStart.ToString();

            postionsound.Play();
            Color newColor = new Color32(58, 202, 179, 255);
            fuego.startColor = newColor;


            /**IMAGEN DE POSCION***/
            Image imagen = m_Image.GetComponent<Image>();
            imagen.sprite = m_Sprite;
            imagen.SetNativeSize();
        }

        void Awake()
        {

            maquinadeEstados = GetComponent<MaquinadeEstados>();

        }
        void checkTurno()
        {
            if (calderoobject.caldero.turnoJuego == ClienteID)
            {
                turno = true;
            }
        }

        // Update is called once per frame  - Check if Colider is in another GameObject
        void Update()
        {
            checkTurno();

            if (timeStart > 0.0)
            {
                timerText.text = Mathf.Round(timeStart).ToString();
                timeStart -= Time.deltaTime;
            }
            else
            {
                isOnTime = false;
                calderoobject.caldero.estado = 5;
                maquinadeEstados.ActivarEstado();
            }

            // if (!turno) return;
            calderoPosition = Caldero.transform.position;
            // boxColider elemento 1
            elemento1Position = Posion1.transform.position;
            // boxColider elemento 2
            elemento2Position = Posion2.transform.position;

            int elemento = 0;
            float distacia1 = 10;

            if (Elemento1.elemento.id == 1)
            {
                elemento = 1;
                distacia1 = distancia_posion(calderoPosition, elemento1Position);
            }
            else if (Elemento2.elemento.id == 1)
            {
                elemento = 1;
                distacia1 = distancia_posion(calderoPosition, elemento2Position);
            }
            else
            {
                elemento = 2;
            }

            compartirVec = Compartir.transform.position;
            //Si es el elemento id = 1 y colisiono con el caldero y esta en tiempo
            if (calderoobject.caldero.isTurno && elemento == 1 && distacia1 < heatingDistance && isOnTime)
            {

                calderoobject.caldero.cambiarEstado(4);
                maquinadeEstados.ActivarEstado();

            }

            // SOLO PUEDE COMPARTIR EL ELEMENTO 1
            float distanciaCompartir = distancia_posion(elemento1Position, compartirVec);


            if (distanciaCompartir < heatingDistance)
            {
                Debug.LogWarning("Colisiono compartir");
                //elemento que quiero compartir
                calderoobject.caldero.elemento = Elemento1.elemento.id;

            }




        }
    }
}