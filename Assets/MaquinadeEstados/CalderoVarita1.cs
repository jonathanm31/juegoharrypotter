﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using System.Linq;
using System;
using System.Text;
using UnityEngine.UI;

namespace Proyect.Networking
{
    public class CalderoVarita1 : MonoBehaviour
    {
        private MaquinadeEstados maquinadeEstados;
        [SerializeField]
        public Material m_Material;
        public GameObject Fluido;
        public GameObject Caldero;
        public GameObject varita;
        public ParticleSystem Circulo;
        public ParticleSystem fuego;
        public ParticleSystem fuegolisto;
        public AudioSource postionsound;
        private Vector3 punto1 = new Vector3();
        private Vector3 punto2 = new Vector3();
        private float heatingDistance = 0.1f;
        private Material materia_anterior;
        public AudioSource musica;
        public AudioSource musicaInicial;

        public float timeStart = 60;
        public Text timerText;
        private int scoreStart;
        public Text scoreText;
        public GameObject m_Image;
        public Sprite m_Sprite;
        private bool isOnTime = true;
        private bool iswinner = false;

        private string[] m_Keywords;
        private KeywordRecognizer m_Recognizer;
        private Dictionary<string, Action> actions = new Dictionary<string, Action>();

        private bool estado = false;

        public CalderoObject calderoobject;

        private bool turno = false;
        public static string ClienteID { get; private set; }

        private float distancia_posion(Vector3 punto1, Vector3 punto2)
        {
            return Mathf.Sqrt(Mathf.Pow(punto1.y - punto2.y, 2));
        }


        private void Mezcla()
        {

            estado = true;
            Circulo.Play();
            musica.Stop();
            musicaInicial.Play();
            iswinner = true;
            scoreStart = int.Parse(scoreText.text);
            scoreStart += 1;


            scoreText.text = scoreStart.ToString();

        }

        private void OnPhraseRecognized(PhraseRecognizedEventArgs args)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0} ({1}){2}", args.text, args.confidence, Environment.NewLine);
            builder.AppendFormat("\tTimestamp: {0}{1}", args.phraseStartTime, Environment.NewLine);
            builder.AppendFormat("\tDuration: {0} seconds{1}", args.phraseDuration.TotalSeconds, Environment.NewLine);
            Debug.Log(builder.ToString());
            actions[args.text].Invoke();

        }



        void Start()
        {
            Circulo.Stop();
            actions.Add("mezcla", Mezcla);
            m_Recognizer = new KeywordRecognizer(actions.Keys.ToArray());

            // m_Recognizer = new KeywordRecognizer(m_Keywords);
            m_Recognizer.OnPhraseRecognized += OnPhraseRecognized;
            m_Recognizer.Start();

            /***TEXTO TIEMPO**/
            timerText.text = timeStart.ToString();
            scoreText.text = scoreStart.ToString();

            /**IMAGEN DE POSCION***/
            Image imagen = m_Image.GetComponent<Image>();
            imagen.sprite = m_Sprite;
            imagen.SetNativeSize();

            Color newColor = new Color32(236, 23, 42, 255);
            fuego.startColor = newColor;
            postionsound.Play();
            Renderer fluido_render = Fluido.GetComponent<Renderer>();
            fluido_render.material = m_Material;

        }


        void Awake()
        {

            maquinadeEstados = GetComponent<MaquinadeEstados>();

        }
        void checkTurno()
        {
            if (calderoobject.caldero.turnoJuego == ClienteID)
            {
                turno = true;
            }
        }

        // Update is called once per frame  - Check if Colider is in another GameObject
        void Update()
        {
            checkTurno();
           // if (!turno) return;
            Debug.LogWarning(estado);
            if (estado && isOnTime)
            {
                fuego.Stop();
                fuegolisto.Play();
                Renderer fluido_render = Fluido.GetComponent<Renderer>();
                fluido_render.material = m_Material;
            }

            if (timeStart > 0.0 && !iswinner)
            {
                timerText.text = Mathf.Round(timeStart).ToString();
                timeStart -= Time.deltaTime;
            }
            else if (iswinner)
            {
                //
            }
            else
            {
                isOnTime = false;
                calderoobject.caldero.estado = 5;
                maquinadeEstados.ActivarEstado();
            }



        }
    }
}
