﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Proyect.Networking
{
    public class MaquinadeEstados : MonoBehaviour
    {
        public MonoBehaviour CalderoEspera;
        public MonoBehaviour CalderoInicial;
        public MonoBehaviour Calderofinal;
        public MonoBehaviour CalderoError;
        public MonoBehaviour CalderoPosion1;
        public MonoBehaviour CalderoPosion2;
        public MonoBehaviour CalderoVarita1;
        private MonoBehaviour [] estados;
        //public MonoBehaviour Timer;

        private MonoBehaviour estadoActual;
        public CalderoObject calderoobject;
        private int oldstate = 0;
        void Start()
        {

            estados = new MonoBehaviour[6];
            estados[1] = CalderoInicial;
            estados[2] = CalderoPosion1;
            estados[3] = CalderoPosion2;
            estados[4] = CalderoVarita1;
            estados[5] = CalderoError;
            MonoBehaviour nuevoEstado = estados[1];
            oldstate = 1;
            estadoActual = nuevoEstado;
            estadoActual.enabled = true;
        }

        public void ActivarEstado()
        {

                 estadoActual.enabled = false;
                 MonoBehaviour nuevoEstado = estados[calderoobject.caldero.estado];
                 estadoActual = nuevoEstado;
                 estadoActual.enabled = true;
                 oldstate = calderoobject.caldero.estado;
                         
        }

        void Update()
        {
           if(oldstate < calderoobject.caldero.estado)
            {
                ActivarEstado();
            }
        }

    }
}
