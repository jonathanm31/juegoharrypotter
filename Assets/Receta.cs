﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using System.Linq;
using System;
using System.Text;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Receta : MonoBehaviour
{


    private string[] m_Keywords;
    private KeywordRecognizer m_Recognizer;
    private Dictionary<string, Action> actions = new Dictionary<string, Action>();

    // Start is called before the first frame update
    void Start()
    {

        actions.Add("cerrar", Cerrar);
        actions.Add("jugar", Jugar);
        actions.Add("tutorial", Tutorial);
        m_Recognizer = new KeywordRecognizer(actions.Keys.ToArray());

        // m_Recognizer = new KeywordRecognizer(m_Keywords);
        m_Recognizer.OnPhraseRecognized += OnPhraseRecognized;
        m_Recognizer.Start();
    }
    private void OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        StringBuilder builder = new StringBuilder();
        builder.AppendFormat("{0} ({1}){2}", args.text, args.confidence, Environment.NewLine);
        builder.AppendFormat("\tTimestamp: {0}{1}", args.phraseStartTime, Environment.NewLine);
        builder.AppendFormat("\tDuration: {0} seconds{1}", args.phraseDuration.TotalSeconds, Environment.NewLine);
        Debug.Log(builder.ToString());
        actions[args.text].Invoke();

    }

    private void Cerrar()
    {

      
    }
    

    private void Jugar()
    {

            SceneManager.LoadScene(0);
  
        
    }
    private void Tutorial()
    {

            SceneManager.LoadScene(0);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
