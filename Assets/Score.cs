﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public float scoreStart = 5;
    public Text scoreText;

    // Start is called before the first frame update
    void Start()
    {
        scoreText.text = scoreStart.ToString();
    }

    public void hasWinning()
    {
        scoreStart += 1;
    }

    public void hasLoss()
    {
        scoreStart -= 1;
    }
    // Update is called once per frame
    void Update()
    {
        scoreText.text = scoreStart.ToString();
    }
}
