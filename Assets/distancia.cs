﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class distancia : MonoBehaviour
{
    [SerializeField]
    public GameObject Obj1;
    public GameObject Obj2;

    public Vector3 punto1 = new Vector3 ();
    public Vector3 punto2 = new Vector3();
    public float teapotToFireDistance = 0.0f;
    public float heatingDistance = 0.0f;


    private float distancia_posion(Vector3 punto1, Vector3  punto2)
    {
        return Mathf.Sqrt( Mathf.Pow(punto1.y - punto2.y, 2));
    }

    void Start()
    {
       
        punto1 = Obj1.transform.position;
        punto2 = Obj2.transform.position;
        teapotToFireDistance = distancia_posion( punto1,  punto2);
        heatingDistance = teapotToFireDistance - 0.02f;
      
    }

    // Update is called once per frame
    void Update()
    {
        punto1 = Obj1.transform.position;
        punto2 = Obj2.transform.position;
        teapotToFireDistance = distancia_posion(punto1, punto2);


        if (teapotToFireDistance < heatingDistance)
            {

              //  Debug.LogWarning(teapotToFireDistance);

            }
            else
            {

                

            }

        
    }
}
