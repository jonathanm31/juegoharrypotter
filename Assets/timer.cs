﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class timer : MonoBehaviour
{
    public float timeStart = 60;
    public Text timerText;
    public bool isOnTime = true;
    // Start is called before the first frame update
    void Start()
    {
        timerText.text = timeStart.ToString();


    }

    // Update is called once per frame
    void Update()
    {
        
        if(timeStart > 0.0)
        {
            timerText.text = Mathf.Round(timeStart).ToString();
            timeStart -= Time.deltaTime;
        }
        else
        {
            isOnTime = false;
        }
        

    }
}
