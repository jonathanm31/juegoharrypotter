﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class visibilidad : MonoBehaviour
{
    public void activarVisibilidad()
    {
        Renderer render = gameObject.GetComponent<Renderer>();
        if(render.enabled )
        {
            render.enabled = false;
        }
        else
        {
            render.enabled = true;
        }
    }
}
